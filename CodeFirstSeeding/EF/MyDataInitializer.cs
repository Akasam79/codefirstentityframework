﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstSeeding.Models;
using System.Data.Entity;
using System.Data.Entity.Migrations;


namespace CodeFirstSeeding.EF
{
    class MyDataInitializer: DropCreateDatabaseAlways<AutoLotEntity>
    {
        protected override void Seed(AutoLotEntity context)
        {
            base.Seed(context);
            var AddUsers = new List<User>
            {
                new User {Name = "Tochukwu", Email = "Tochukwu@gmail.com"},
                new User {Name = "Shola", Email = "shola.nejo@domain.com" },
                new User {Name = "Chikky", Email = "chikky@domain.com" },
                new User {Name = "Dara", Email = "dara@domain.com" },
                new User { Name = "Alex", Email = "alex@domain.com" },
                new User { Name = "Uriel", Email = "Uriel@domain.com" },
                new User { Name = "Kachi", Email = "kachi@domain.com" },
                new User { Name = "Chinedu", Email = "chinedu@domain.com" },
                new User { Name = "Loveth", Email = "loveth@domain.com" },
                new User { Name = "Sunday", Email = "sunday@domain.com" },
                new User { Name = "Sammy", Email = "sammy@domain.com" },
                new User { Name = "Einstien", Email = "einstien @domain.com" },
                new User { Name = "KCM", Email = "kcm@domain.com" },
                new User { Name = "Obinna", Email = "obi@domain.com" },
                new User { Name = "Gideon", Email = "Giddy@domain.com" },
                new User { Name = "Francis", Email = "Francis2@domain.com" },
            };
            AddUsers.ForEach(x => context.Users.AddOrUpdate(c => new { c.Name, c.Email }, x));
            context.SaveChanges();

            var addAccount = new List<Account>
            {
                new Account {UserId = 1, Account_Number =  66515201, Balance = 100001},
                new Account {UserId = 2, Account_Number =  66515202, Balance = 100002},
                new Account {UserId = 3, Account_Number =  66515203, Balance = 100003},
                new Account {UserId = 4, Account_Number =  66515204, Balance = 100004},
                new Account {UserId = 5, Account_Number =  66515205, Balance = 100005},
                new Account {UserId = 6, Account_Number =  66515206, Balance = 100006},
                new Account {UserId = 7, Account_Number =  66515207, Balance = 100007},
                new Account {UserId = 8, Account_Number =  66515208, Balance = 100008},
                new Account {UserId = 9, Account_Number =  66515209, Balance = 100009},
                new Account {UserId = 10, Account_Number = 66515210, Balance = 100010},
                new Account {UserId = 11, Account_Number = 66515211, Balance = 100011},
                new Account {UserId = 12, Account_Number = 66515212, Balance = 100012},
                new Account {UserId = 13, Account_Number = 66515213, Balance = 100013},
                new Account {UserId = 14, Account_Number = 66515214, Balance = 100014},
                new Account {UserId = 15, Account_Number = 66515215, Balance = 100015},
                new Account {UserId = 16, Account_Number = 66515216, Balance = 100016},
            };
            context.Accounts.AddOrUpdate(a => new { a.UserId, a.Account_Number, a.Balance }, addAccount.ToArray());
            context.SaveChanges();
        }
    }
}
