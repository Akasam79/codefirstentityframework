﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CodeFirstSeeding.Models;
using CodeFirstSeeding.EF;

namespace CodeFirstSeeding
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("**** Fun with Entity Framework****\n");

            using (var context = new AutoLotEntity())
            {
                foreach (User u in context.Users)
                {
                    Console.WriteLine(u.Name, u.Email);
                }
            }
            Console.ReadLine();
        }
    }
}
