﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoLotConsoleApp2.EF;
using System.Data.Entity;

namespace AutoLotConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //AddNewRecord();

            SelectAll();

            //RemoveRecord(18);
            //Console.WriteLine();

            //SelectAll();

            //User[] users2add =
            //{
            //    new User ()
            //    {
            //        Name = "name1",
            //        Email = "email1"
            //    },
            //    new User ()
            //    {
            //        Name = "name2",
            //        Email = "email2"
            //    },
            //    new User ()
            //    {
            //        Name = "name3",
            //        Email = "email3"
            //    }
            //};
            //AddMany(users2add);

            UpdateRecord(19);
            
            SelectAll();
        }

        private static void AddNewRecord()
        {
            using (var contxt = new AutoLotEntity())
            {
                var user = new User()
                {

                    Name = "Ebuka3",
                    Email = "NewEmail3",
                };
                contxt.Users.Add(user);
                contxt.SaveChanges();
            }
        }

        private static void AddMany(IEnumerable<User> UserToAdd)
        {
            using (var context = new AutoLotEntity())
            {
                context.Users.AddRange(UserToAdd);
                context.SaveChanges();
            }
        }
        private static void SelectAll()
        {
            using (var context = new AutoLotEntity())
            {
                foreach (User user in context.Users)
                {
                    Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
                }
            }
        }

        private static void RemoveRecord(int userId)
        {
            using (var context = new AutoLotEntity())
            {
                User userToDelete = context.Users.Find(userId);

                if(userToDelete != null)
                {
                    context.Users.Remove(userToDelete);
                    
                    if(context.Entry(userToDelete).State != EntityState.Deleted)
                    {
                        throw new Exception("Unable to delete record o, no vex");
                    }
                    context.SaveChanges();
                    Console.WriteLine("Record has been deleted o, Ok, see what we have now if you don't believe me.....");
                }

                else
                {
                    Console.WriteLine();
                    Console.WriteLine( $"\t \b user with id {userId} not found");
                }
            }
        }

        private static void UpdateRecord(int userId)
        {
            using (var context = new AutoLotEntity())
            {
                User userToUpdate = context.Users.Find(userId);

                if (userToUpdate != null)
                {
                    Console.WriteLine(context.Entry(userToUpdate).State); 
                        userToUpdate.Name = "Samuel";
                    if (context.Entry(userToUpdate).State != EntityState.Modified)
                    {
                        throw new Exception("Unable to Modify record o, no vex");
                    }
                    context.SaveChanges();
                    Console.WriteLine("Record has been updated o, Ok, see what we have now if you don't believe me.....");
                }

                else
                {
                    Console.WriteLine();
                    Console.WriteLine($"\t \b user with id {userId} not found");
                }
            }
        }

    }
}
